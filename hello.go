package main

import (
	"bufio"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"os"
	"strings"
	"time"
)

const MONITORAMENTOS = 10
const DELAYENTRETESTES = 5

func main() {

	exibeIntroducao()
	for {
		exibeMenu()
		comando := leComando()

		switch comando {
		case 1:
			iniciarMonitoramento()
		case 2:
			imprimeLogs()
		case 0:
			fmt.Println("Saindo do programa")
			os.Exit(0)
		default:
			fmt.Println("Opção selecionada inválida")
			os.Exit(-1)
		}
	}
}

func exibeIntroducao() {
	nome := "Pedro"
	versao := 1.1

	fmt.Println("Ola Sr,", nome)
	fmt.Println("Esta programa esta na versão,", versao)
}

func exibeMenu() {
	fmt.Println("1- Iniciar Monitoramento")
	fmt.Println("2- Exibir Logs")
	fmt.Println("0- Sair do Programa")
}

func leComando() int {
	var comandoLido int
	fmt.Scan(&comandoLido)
	fmt.Println("O comando escolhido foi,", comandoLido)
	return comandoLido
}

func iniciarMonitoramento() {
	fmt.Println("Monitorando...")
	// sites := pegaSitesParaMonitorar()

	sites, err := pegaSitesParaMonitorarDeUmArquivoTxt()
	if err != nil {
		fmt.Println("Erro ao testar" + err.Error())
	}

	for x := 0; x < MONITORAMENTOS; x++ {
		for i, site := range sites {
			fmt.Println("Indice: ", i)
			resultado := testaSite(site)
			fmt.Println(resultado)
		}
		time.Sleep(DELAYENTRETESTES * time.Second)
	}
}

func pegaSitesParaMonitorar() []string {
	sites := []string{
		"https://alura-forum-2022-env-staging.herokuapp.com/topicos/",
		"https://alura-forum-2022-env-prod.herokuapp.com/topicos/",
		"https://alura-tracker.netlify.app/#/",
		"https://amazon-clone-vuejs.netlify.app/",
		"https://api-amazon-clone-marcelosilva.herokuapp.com/",
	}

	return sites
}

func testaSite(site string) string {
	resp, err := http.Get(site)

	if err != nil {
		return "Erro ao testar" + err.Error()
	}

	if resp.StatusCode == 200 {
		mensagem := "Sucesso ao acessar site " + site + " Status code:" + fmt.Sprint(resp.StatusCode)
		registraLog(mensagem)
		return mensagem
	} else {
		mensagem := "Erro ao acessar site " + site + " Status code:" + fmt.Sprint(resp.StatusCode)
		registraLog(mensagem)
		return mensagem
	}
}

func pegaSitesParaMonitorarDeUmArquivoTxt() ([]string, error) {
	var sites []string
	arquivo, err := os.Open("sites.txt")
	if err != nil {
		return nil, err
	}
	// arquivo, err := ioutil.ReadFile("sites.txt")
	leitor := bufio.NewReader(arquivo)

	for {
		linha, err2 := leitor.ReadString('\n')
		linha = strings.TrimSpace(linha)
		sites = append(sites, linha)

		if err2 == io.EOF {
			break
		}
	}

	arquivo.Close()

	return sites, nil
}

func registraLog(msg string) {
	arquivo, err := os.OpenFile("log.txt", os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)

	if err != nil {
		fmt.Println(err)
	}

	arquivo.WriteString(time.Now().Format("02/01/2006 15:04:05") + " - " + msg + "\n")
	arquivo.Close()
}

func imprimeLogs() {
	fmt.Println("Exibindo Logs...")
	arquivo, err := ioutil.ReadFile("log.txt")

	if err != nil {
		fmt.Println(err)
	}

	fmt.Println(string(arquivo))
}
